Live Demo: http://185.65.244.151/

Installation:

git clone git@bitbucket.org:ilysenko/cc.git
cd cc
virtualenv ENV --no-site-packages
source ENV/bin/activate
pip install -r requirements.txt
cd cc_charts/
python manage.py runserver

the server is available at http://127.0.0.1:8000

To import new data to database from csv file use:

python manage.py importcsv --mappings='' --model='cc_api.BuildLogItem' FILENAME

Tested:
  IE >= 9.0
  all modern browsers
