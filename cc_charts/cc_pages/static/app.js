var app = angular.module('cc_chart', ['ngResource', 'chart.js', 'mgcrea.ngStrap']);

app.factory('LogItems', function ($resource, $http, $rootScope) {
    // API Factory
    var minData, maxData;
    return {
        get: function (params) {
            return $http.get('/api/logitems/', {params: params}).
                then(function (data) {
                        // initializing min and max dates
                        minData = data.data.labels[0];
                        maxData = data.data.labels[data.data.labels.length - 1];
                        $rootScope.$broadcast('dates:updated', [minData, maxData])
                    return data;
                })
        },
        durations: function (params) {
            return $http.get('/api/durations/', {params: params})
        },
        abnormal: function (params) {
            return $http.get('/api/abnormal/', {params: params})
        }
    }
});

app.controller("ccCharCtrl", function ($scope, $rootScope, LogItems) {
    $scope.type = 'Bar';

    $scope.loadData = function (params) {
        LogItems.get(params).then(function (res) {
                $scope.labels = res.data.labels;
                $scope.data = res.data.data;
                $scope.series = ['Passed', 'Failed'];
            }
        );
    };
    $scope.loadData();

    $rootScope.$on("refreshCharts", function (args, data) {
        $scope.loadData(data.params);
    });

    $scope.toggle = function () {
      $scope.type = $scope.type == 'Bar' ?  'Line' : 'Bar';
    };
});

app.controller("ccDurationsCtrl", function ($scope, $rootScope, LogItems) {

    $scope.loadData = function (params) {
        LogItems.durations(params).then(function (res) {
                $scope.labels = res.data.labels;
                $scope.data = [res.data.data];
            }
        );
    };
    $scope.loadData();

    $rootScope.$on("refreshCharts", function (args, data) {
        $scope.loadData(data.params);
    });
});

app.controller("ccAbnormalCtrl", function ($scope, $rootScope, LogItems) {

    $scope.loadData = function (params) {
        LogItems.abnormal(params).then(function (res) {
                $scope.labels = res.data.labels;
                $scope.data = res.data.data;
                $scope.series = ['Passed', 'Failed', 'Abnormal'];
            }
        );
    };
    $scope.loadData();

    $rootScope.$on("refreshCharts", function (args, data) {
        $scope.loadData(data.params);
    });
});

app.controller("ccDateCtrl", function ($scope, $rootScope, $datepicker, LogItems) {

    $scope.refreshCharts = function (fromDate, toDate) {
        $rootScope.$broadcast("refreshCharts", {
            params: {from: fromDate, to: toDate}
        });
    };

    $scope.resetCharts = function () {
        $rootScope.$broadcast("refreshCharts", {
            params: {}
        });
    };

    $scope.$on('dates:updated', function (event, data) {
        $scope.minDate = $scope.fromDate = data[0];
        $scope.maxDate = $scope.toDate = data[1];
    });

});