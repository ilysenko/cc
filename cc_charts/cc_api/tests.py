# coding: utf-8
import unittest
import mock
from datetime import datetime
from cc_api import  views


TEST_DATA = [
    {'created_at': datetime.strptime('2014-10-9 05:38:55 UTC', '%Y-%m-%d %H:%M:%S %Z'),
     'summary_status': 'passed',
     'duration': 500},
    {'created_at': datetime.strptime('2014-10-10 05:38:55 UTC', '%Y-%m-%d %H:%M:%S %Z'),
     'summary_status': 'failed',
     'duration': 600},
    {'created_at': datetime.strptime('2014-10-10 05:38:55 UTC', '%Y-%m-%d %H:%M:%S %Z'),
     'summary_status': 'error',
     'duration': 700},
]


TEST_DATA_NOT_ABNORMAL = {'data': [[1, 0], [0, 2]],
                          'labels': [datetime(2014, 10, 9).date(),
                                     datetime(2014, 10, 10).date()]}


TEST_DATA_ABNORMAL = {'data': [[1, 0], [0, 2], [0, 0]],
                          'labels': [datetime(2014, 10, 9).date(),
                                     datetime(2014, 10, 10).date()]}


def prepare_mock_data(data):
    res = []
    for i in data:
        res.append(mock.Mock(**i))
    return res


class SecureViewTestCase(unittest.TestCase):
    """
    Unit Test example
    """
    def test_api(self):
        mock_data = prepare_mock_data(TEST_DATA)
        with mock.patch('cc_api.views.CACHED_QUERY', mock_data):

            self.assertDictEqual(views.stacked_chart_data(),
                                 TEST_DATA_NOT_ABNORMAL)

            self.assertDictEqual(views.stacked_chart_data(add_abnormal=True),
                                 TEST_DATA_ABNORMAL)

