# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BuildLogItem',
            fields=[
                ('session_id', models.IntegerField(default=0, serialize=False, primary_key=True)),
                ('started_by', models.CharField(max_length=100, null=True, blank=True)),
                ('created_at', models.DateTimeField(null=True, blank=True)),
                ('summary_status', models.CharField(default=b'', max_length=7, blank=True)),
                ('duration', models.DecimalField(default=0, null=True, max_digits=10, decimal_places=5, blank=True)),
                ('worker_time', models.DecimalField(default=0, null=True, max_digits=82, decimal_places=10, blank=True)),
                ('bundle_time', models.NullBooleanField(default=False)),
                ('num_workers', models.IntegerField(default=0, null=True, blank=True)),
                ('branch', models.CharField(default=b'', max_length=10, blank=True)),
                ('commit_id', models.CharField(max_length=100, null=True, blank=True)),
                ('started_tests_count', models.NullBooleanField(default=False)),
                ('passed_tests_count', models.NullBooleanField(default=0)),
                ('failed_tests_count', models.NullBooleanField(default=False)),
                ('pending_tests_count', models.IntegerField(default=0, null=True, blank=True)),
                ('skipped_tests_count', models.NullBooleanField(default=False)),
                ('error_tests_count', models.NullBooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
