from itertools import groupby
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from cc_api.models import BuildLogItem
from datetime import datetime


CACHED_QUERY = BuildLogItem.objects.all().order_by('created_at')


def extract_hours(entity):
    """
    Returns hour ('00'...'24') from BuildLogItem instance
    :param entity:
        BuildLogItem
    :return:
        String
    """
    return entity.created_at.strftime('%H')


def extract_date(entity):
    """
    Returns date without time from BuildLogItem instance
    :param entity:
        BuildLogItem
    :return:
        String
    """
    return entity.created_at.date()


def time_chart_data(data):
    items = sorted(data, key=extract_hours)
    durations, labels = [], []

    for hour, group in groupby(items, key=extract_hours):
        total_duration, counter = 0, 0
        for i in group:
            total_duration += i.duration
            counter += 1
        labels.append(hour)
        durations.append(
            float("{0:.1f}".format(total_duration/counter, 0)))

    res =  {
        'data': durations,
        'labels': labels
    }
    return res


def stacked_chart_data(data, add_abnormal=False):
    oks, fails, labels, abnormal = [], [], [], []

    for start_date, group in groupby(data, key=extract_date):
        ok, fail = 0, 0,
        for i in group:
            if i.summary_status == 'passed':
                ok = ok + 1
            else:
                fail = fail + 1
        oks.append(ok)
        fails.append(fail)
        labels.append(start_date)
        if add_abnormal:
            # TODO: have to well algorithm
            # Pls look at http://stats.stackexchange.com/questions/98961/anomaly-detection-what-algorithm-to-use
            if fail > ok/2 and (ok + fail) > 5:
                abnormal.append(fail)
            else:
                abnormal.append(0);

    res =  {
        'data': [oks if oks else [0],
                 fails if fails else [0]],
        'labels': labels
    }
    if add_abnormal:
        res['data'].append(abnormal)
    return res


def prepare_data(params):
    if not params:
        return CACHED_QUERY
    query = {}
    if params.get('from'):
       query['created_at__gte'] = datetime.strptime(params.get('from'), '%Y-%m-%d')
    if params.get('to'):
       query['created_at__lte'] = datetime.strptime(
           params.get('to')+' 23:59', '%Y-%m-%d %H:%M')
    res = BuildLogItem.objects.filter(**query)
    return res


class StackedViewApi(APIView):
    def get(self, request, *args, **kw):
        result = stacked_chart_data(prepare_data(request.GET))
        response = Response(result, status=status.HTTP_200_OK)
        return response


class DurationsViewApi(APIView):
    def get(self, request, *args, **kw):
        result = time_chart_data(prepare_data(request.GET))
        response = Response(result, status=status.HTTP_200_OK)
        return response


class AbnormalViewApi(APIView):
    def get(self, request, *args, **kw):
        result = stacked_chart_data(prepare_data(request.GET), add_abnormal=True)
        response = Response(result, status=status.HTTP_200_OK)
        return response