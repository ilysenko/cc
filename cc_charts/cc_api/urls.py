from django.conf.urls import patterns, url
from cc_api.views import StackedViewApi, DurationsViewApi, AbnormalViewApi

urlpatterns = patterns('',
    url(r'^logitems/$', StackedViewApi.as_view()),
    url(r'^durations/$', DurationsViewApi.as_view()),
    url(r'^abnormal/$', AbnormalViewApi.as_view()),
)
