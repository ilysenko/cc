from django.db import models


class BuildLogItem(models.Model):
    session_id = models.IntegerField(default=0, null=False, primary_key=True, blank=False)
    started_by = models.CharField(null=True, blank=True, max_length=100)
    created_at = models.DateTimeField(null=True, blank=True)
    summary_status = models.CharField(default='', max_length=7, blank=True)
    duration = models.DecimalField(default=0, null=True, max_digits=10, decimal_places=5, blank=True)
    worker_time = models.DecimalField(default=0, null=True, max_digits=82, decimal_places=10, blank=True)
    bundle_time = models.NullBooleanField(default=False, null=True, blank=True)
    num_workers = models.IntegerField(default=0, null=True, blank=True)
    branch = models.CharField(default='', max_length=10, blank=True)
    commit_id = models.CharField(null=True, blank=True, max_length=100)
    started_tests_count = models.NullBooleanField(default=False, null=True, blank=True)
    passed_tests_count = models.NullBooleanField(default=0, null=True, blank=True)
    failed_tests_count = models.NullBooleanField(default=False, null=True, blank=True)
    pending_tests_count = models.IntegerField(default=0, null=True, blank=True)
    skipped_tests_count = models.NullBooleanField(default=False, null=True, blank=True)
    error_tests_count = models.NullBooleanField(default=False, null=True, blank=True)
