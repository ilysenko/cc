from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from cc_pages.views import IndexPageView

from django.conf import settings

urlpatterns = patterns('',
    url(r'^$', IndexPageView.as_view(), name='home'),
    url(r'^api/', include('cc_api.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
